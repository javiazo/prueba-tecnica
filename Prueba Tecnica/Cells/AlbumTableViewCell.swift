//
//  AlbumTableViewCell.swift
//  Prueba Tecnica
//
//  Created by Javier Azorín Fernández on 20/5/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import UIKit

final class AlbumTableViewCell: UITableViewCell {

    func configureCell(album: Album) {
        // Title
        textLabel?.text = album.title
        
        // Subtitle
        detailTextLabel?.text = "\(NSLocalizedString("id", comment: "")): \(album.id)"
    }
    
}
