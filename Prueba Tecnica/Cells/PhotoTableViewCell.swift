//
//  PhotoTableViewCell.swift
//  Prueba Tecnica
//
//  Created by Javier Azorín Fernández on 20/5/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import UIKit

class PhotoTableViewCell: UITableViewCell {

    func configureCell(photo: Photo) {
        // Placeholder image
        imageView?.image = UIImage(named: "placeholder")
        
        // Title
        textLabel?.text = photo.title
        
        // Subtitle
        detailTextLabel?.text = "\(NSLocalizedString("id", comment: "")): \(photo.id)"
    }

}
