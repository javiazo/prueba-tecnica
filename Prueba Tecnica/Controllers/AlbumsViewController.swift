//
//  AlbumsViewController.swift
//  Prueba Tecnica
//
//  Created by Javier Azorín Fernández on 20/5/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import UIKit

final class AlbumsViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // Segues
    private let SeguePhotos = "photos"
    
    // Albums
    private var albums: [Album] = [] {
        didSet {
            // Update table (main thread)
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
    }
    
    // PhotosAPI
    private let photosAPI = PhotosAPI()
    
    // Cells
    private let CellAlbum = "album"

    
    // MARK: App lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure view
        configureView()
        
        // Get albums
        tableView.hidden = true // Hide table while loading
        photosAPI.albums { (result) in
            self.tableView.hidden = false // Show table when finish
            switch result {
            case .Success(let albumsAux):
                self.albums = albumsAux
                break
            case .Failure(let errStr):
                print(errStr)
                break
            }
        }
    }
    
    // View configuration
    private func configureView() {
        // Title
        title = NSLocalizedString("albums", comment: "")
    }
    
    // MARK Segues
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let segueId = segue.identifier else { return }
        
        switch segueId {
        case SeguePhotos:
            guard let index = tableView.indexPathForSelectedRow?.row else { return } // Get selected row
            guard index < albums.count else { return } // Avoid out of index error
            
            let photosVC = segue.destinationViewController as! PhotosViewController
            photosVC.album = albums[index]
            break
        default: break
        }
    }
}

// MARK: UITableView Data Source
extension AlbumsViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CellAlbum, forIndexPath: indexPath) as! AlbumTableViewCell
        if indexPath.row < albums.count { // Avoid out of index error
            // Cell configuration
            cell.configureCell(albums[indexPath.row])
        }
        return cell
    }
}

// MARK: UITableView Delegate
extension AlbumsViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {        
        if indexPath.row < albums.count { // Avoid out of index error
            // Perform Segue
            performSegueWithIdentifier(SeguePhotos, sender: nil)
        }
    }
}
