//
//  PhotosViewController.swift
//  Prueba Tecnica
//
//  Created by Javier Azorín Fernández on 20/5/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import UIKit

final class PhotosViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // Album (Implicitly Unwrapped Optional)
    var album: Album!
    
    // Photos
    private var photos: [Photo] = [] {
        didSet {
            // Update table (main thread)
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
    }
    
    // PhotosAPI
    private let photosAPI = PhotosAPI()
    
    // Cells
    private let CellPhoto = "photo"

    
    // MARK: App lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure view
        configureView()
        
        // Get photos
        tableView.hidden = true // Hide table while loading
        photosAPI.photos(album.id) { (result) in
            self.tableView.hidden = false // Show table when finish
            switch result {
            case .Success(let photosAux):
                self.photos = photosAux
                break
            case .Failure(let errStr):
                print(errStr)
                break
            }
        }
    }
    
    // View configuration
    private func configureView() {
        // Title
        title = String(format: NSLocalizedString("photos-from-album", comment: ""), album.id)
    }
}

// MARK: UITableView Data Source
extension PhotosViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CellPhoto, forIndexPath: indexPath) as! PhotoTableViewCell
        if indexPath.row < photos.count { // Avoid out of index error
            // Cell configuration
            cell.configureCell(photos[indexPath.row])
            // Image
            photosAPI.imageWithUrl(photos[indexPath.row].thumbnailUrl) { (result) in
                switch result {
                case .Success(let image):
                    dispatch_async(dispatch_get_main_queue()) {
                        cell.imageView?.image = image
                    }
                    break
                case .Failure(let errStr):
                    print(errStr)
                    break
                }
            }
        }
        return cell
    }
}
