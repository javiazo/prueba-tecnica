//
//  Constants.swift
//  Prueba Tecnica
//
//  Created by Javier Azorín Fernández on 20/5/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import Foundation

/// App constants
struct Const {
    /// API Constants
    struct API {
        static let URL = "http://jsonplaceholder.typicode.com/"
        struct Method {
            static let albums = "albums"
            static let photos = "photos"
        }
    }
}
