//
//  PhotosAPI.swift
//  Prueba Tecnica
//
//  Created by Javier Azorín Fernández on 20/5/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import Foundation
import UIKit

// MARK: Result enum (w/ generics)
enum Result<T> {
    case Success(T)
    case Failure(String)
}

// MARK: PhotosAPI
/// Albums & Photos API
struct PhotosAPI {
    
    // Images cache
    private var imageCache =  NSCache()
    
    /**
     Get all the albums
     
     - parameter completion: Call result (Success or Failure)
     */
    func albums(completion: (result: Result<[Album]>) -> Void) {
        // URL String
        let urlString = Const.API.URL + Const.API.Method.albums
        
        // URL
        guard let url = NSURL(string: urlString) else {
            completion(result: .Failure("Error with the URL!"))
            return
        }
        
        // Request
        let request = NSURLRequest(URL: url)
        
        // Call
        NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            // Get the data
            guard let data = data else {
                completion(result: .Failure("Error reciving the data!"))
                return
            }
            
            // Parse the data
            do {
                guard let dics = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as? [[String : AnyObject]] else {
                    completion(result: .Failure("Error parsing the data!"))
                    return
                }
                
                completion(result: .Success(dics.map({ (item) -> Album in
                    Album(dic: item)
                })))
            } catch {
                completion(result: .Failure("Error parsing the data!"))
                return
            }
            }.resume()
    }
    
    /**
     Get photos by album id
     
     - parameter albumId: Album's Id
     - parameter completion: Call result (Success or Failure)
     */
    func photos(albumId: Int, completion: (result: Result<[Photo]>) -> Void) {
        // Arguments
        let methodArguments = [
            "albumId": albumId
        ]
        
        // URL String
        let urlString = Const.API.URL + Const.API.Method.photos + escapedParameters(methodArguments)
        
        // URL
        guard let url = NSURL(string: urlString) else {
            completion(result: .Failure("Error with the URL!"))
            return
        }
        
        // Request
        let request = NSURLRequest(URL: url)
        
        // Call
        NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            // Get the data
            guard let data = data else {
                completion(result: .Failure("Error reciving the data!"))
                return
            }
            
            // Parse the data
            do {
                guard let dics = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as? [[String : AnyObject]] else {
                    completion(result: .Failure("Error parsing the data!"))
                    return
                }
                
                completion(result: .Success(dics.map({ (item) -> Photo in
                    Photo(dic: item)
                })))
            } catch {
                completion(result: .Failure("Error parsing the data!"))
                return
            }
            }.resume()
    }
    
    /**
     Get the image from the URL
     
     - parameter imageUrl: Image url
     - parameter completion: Call result (Success or Failure)
     */
    func imageWithUrl(imageUrl: String, completion: (result: Result<UIImage>) -> Void) {
        if let image = imageCache.objectForKey(imageUrl) {
            // Image from cache
            guard let image = image as? UIImage else {
                completion(result: .Failure("Error getting the image from cache!"))
                return
            }
            
            // Return the image
            completion(result: .Success(image))
        } else {
            // Image from URL
            guard let url = NSURL(string: imageUrl) else {
                completion(result: .Failure("Error with the image url!"))
                return
            }
            
            // Request
            let request = NSURLRequest(URL: url)
            
            // Call
            NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
                // Get the data
                guard let data = data else {
                    completion(result: .Failure("Error reciving the data!"))
                    return
                }
                
                guard let image = UIImage(data: data) else {
                    completion(result: .Failure("Error creating the image from data!"))
                    return
                }
                
                // Save the image in cache
                self.imageCache.setObject(image, forKey: imageUrl)
                
                // Return the image
                completion(result: .Success(image))
                }.resume()
        }
    }
    
    // MARK: - Escaped parameters
    /**
     Creates a String with all the parameters for a URL
     
     - parameter dic: Dictionary of key:value parameters
     */
    private func escapedParameters(dic: [String : AnyObject]) -> String {
        guard dic.count > 0 else { return "" }
        
        var str = "?"
        
        for (index, item) in dic.enumerate() {
            str = str + "\(item.0)=\(item.1)"
            if index < dic.count - 1 {
                str = str + "&"
            }
        }
        
        return str
    }
}
