//
//  Album.swift
//  Prueba Tecnica
//
//  Created by Javier Azorín Fernández on 20/5/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import Foundation

struct Album {
    var userId: Int
    var id: Int
    var title: String
    
    init(dic: [String : AnyObject]) {
        userId = dic["userId"] as? Int ?? -1
        id = dic["id"] as? Int ?? -1
        title = dic["title"] as? String ?? ""
    }
}
