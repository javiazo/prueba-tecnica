//
//  Photo.swift
//  Prueba Tecnica
//
//  Created by Javier Azorín Fernández on 20/5/16.
//  Copyright © 2016 Javier Azorín Fernández. All rights reserved.
//

import Foundation

struct Photo {
    var albumId: Int
    var id: Int
    var title: String
    var url: String
    var thumbnailUrl: String
    
    init(dic: [String : AnyObject]) {
        albumId = dic["albumId"] as? Int ?? -1
        id = dic["id"] as? Int ?? -1
        title = dic["title"] as? String ?? ""
        url = dic["url"] as? String ?? ""
        thumbnailUrl = dic["thumbnailUrl"] as? String ?? ""
    }
}
